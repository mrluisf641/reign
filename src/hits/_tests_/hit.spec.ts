import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import {
  closeInMongodConnection,
  rootMongooseTestModule,
} from './test-utils/mongo/MongooseTestModule';
import { HitSchema } from '../models/hit.model';
import HitRepository from '../hit.repository';
import { HitService } from '../hit.service';

describe('SquidService', () => {
  let repository: HitRepository;
  let service: HitService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        MongooseModule.forFeature([{ name: 'Hit', schema: HitSchema }]),
      ],
      providers: [HitRepository, HitService],
    }).compile();

    repository = module.get<HitRepository>(HitRepository);
    service = module.get<HitService>(HitService);
  });

  it('should be defined', () => {
    expect(repository).toBeDefined();
    expect(service).toBeDefined();
  });

  /**
    Write meaningful test
  **/

  afterAll(async () => {
    await closeInMongodConnection();
  });
});
