import { HttpStatus, Injectable, Param, Res } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Response } from 'express';
import { Hit, HitDocument } from './models/hit.model';

@Injectable()
export default class HitRepository {
  constructor(@InjectModel(Hit.name) private hitModel: Model<HitDocument>) {}

  async index(@Res() res: Response): Promise<Response<HitDocument[]>> {
    const listHits = await this.hitModel.find({});
    return res.status(HttpStatus.OK).json({
      message: 'list hits recivied sucessfully',
      body: listHits,
    });
  }

  async destroy(@Res() res: Response, @Param() id: string) {
    const hit = this.hitModel.findById(id);

    hit.remove({ _id: id }, (err) => {
      if (!err) {
        return res.status(HttpStatus.OK).json({
          message: 'Deleted successfully',
        });
      } else {
        throw err;
      }
    });
  }
}
