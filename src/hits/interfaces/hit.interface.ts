export interface _highlightResultI {
  author: {
    value: string;
    matchLevel: string;
    matchedWords: [];
  };
  comment_text: {
    value: string;
    matchLevel: string;
    fullyHighlighted: boolean;
    matchedWords: string[];
  };
  story_title: {
    value: string;
    matchLevel: string;
    matchedWords: [];
  };
  story_url: {
    value: string;
    matchLevel: string;
    matchedWords: [];
  };
}
