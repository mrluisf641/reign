import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { _highlightResultI } from '../interfaces/hit.interface';

export type HitDocument = Hit & Document;

@Schema()
export class Hit {
  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop()
  author: string;

  @Prop()
  points: string;

  @Prop()
  story_text: string;

  @Prop()
  comment_text: string;

  @Prop()
  num_comments: number;

  @Prop()
  story_id: number;

  @Prop()
  story_title: string;

  @Prop()
  story_url: string;

  @Prop()
  parent_id: number;

  @Prop()
  created_at_i: number;

  @Prop()
  created_at: Date;

  @Prop()
  _tags: string[];

  @Prop()
  objectID: string;

  @Prop({ type: 'Object' })
  _highlightResult: _highlightResultI;
}

export const HitSchema = SchemaFactory.createForClass(Hit);
