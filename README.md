## Description

Hello. I have created this repository based on NestJS. It is necessary to already have mongoDB installed on your computer. Just follow these commands.

### Env

Before you will need to adjust the environment variables, based on how you install the app

```bash
$ cp .env.example .env

```
## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Installation With Docker

Now, to use it with docker they need to have this package installed, and docker compose. They perform the following commands:

```bash
# This will generate the necessary build for the application
$ docker-compose build

# And finally the application will start
$ docker-compose up

```